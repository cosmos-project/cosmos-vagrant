# COSMOS Vagrant #

Use this repository to install COSMOS using Vagrant (a virtual machine manager).

### How do I get set up? ###

* Install Vagrant
* Clone repository
* $vagrant up

open the image with 

* $vagrant ssh

start agent 001

* $agent_001

open another terminal window
open the same image with 

* $vagrant ssh

start agent 002

* $agent_002


